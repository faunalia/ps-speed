# -*- coding: utf-8 -*-

"""
/***************************************************************************
Name                : PS Time Series Viewer
Description         : Computation and visualization of time series of speed for
                    Permanent Scatterers derived from satellite interferometry
Date                : Jul 25, 2012
copyright           : (C) 2012 by Giuseppe Sucameli and Matteo Ghetta (Faunalia)
email               : brush.tyler@gmail.com matteo.ghetta@faunalia.it

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

from qgis.PyQt import uic

from qgis.PyQt.QtCore import (
    QDate,
    QCoreApplication,
    pyqtSignal
)

from qgis.PyQt.QtWidgets import (
    QDialog,
    QVBoxLayout,
    QCheckBox,
    QMessageBox
)

from qgis.PyQt.QtGui import QColor

from matplotlib.backends.backend_qt5agg import (
    FigureCanvasQTAgg,
    NavigationToolbar2QT as NavigationToolbar
)
from matplotlib.figure import Figure
import matplotlib
from matplotlib.dates import date2num
from functools import partial

import numpy as np
from statistics import mean
import random
import webbrowser

import os

from qgis.core import (
    QgsFeatureRequest,
    QgsExpressionContext,
    QgsExpressionContextUtils,
    QgsApplication,
    QgsExpression,
    Qgis,
    QgsProject
)

from qgis.gui import (
    QgsColorButton,
    QgsSpinBox
)



from ..utils import getXYvalues, CustomRectangleMapTool
from collections import defaultdict


class MplCanvas(FigureCanvasQTAgg):

    def __init__(self, parent=None, width=5, height=4, dpi=100):
        self.fig = Figure(figsize=(width, height), dpi=dpi)
        self.axes = self.fig.add_subplot(111)
        super(MplCanvas, self).__init__(self.fig)


path = os.path.join(os.path.dirname(__file__), 'create_plot_dialog.ui')

FORM_CLASS, _ = uic.loadUiType(path)

class createPlotDialog(QDialog, FORM_CLASS):

    plot_created = pyqtSignal()

    def __init__(self, iface, x, y, fieldMap, fid, layer, parent=None):
        super().__init__(parent)
        self.setupUi(self)

        self.iface = iface
        # store the x and y values
        self.x = x
        self.y = y
        self.fieldMap = fieldMap
        self.fid = fid
        self.layer = layer

        self.plots = defaultdict(lambda: defaultdict(dict))
        self.plots[self.fid]['values']['x'] = self.x
        self.plots[self.fid]['values']['y'] = self.y

        # get the X min and max values
        min_x = min(self.x)
        max_x = max(self.x)

        # get the Y min and max values
        min_y = min(self.y)
        max_y = max(self.y)

        # connect the plot default signal to the method
        self.plot_created.connect(self.enableMultiplePlotChoice)

        # connect the radio buttons to the method to enable the error bar
        self.singlePlotRadio.clicked.connect(self.enableErrorBarChoice)
        self.multiplePlotRadio.clicked.connect(self.enableErrorBarChoice)

        # set the X datetime limits
        self.minDateEdit.setDate(QDate(min_x.year, min_x.month, min_x.day))
        self.maxDateEdit.setDate(QDate(max_x.year, max_x.month, max_x.day))

        # set the Y limits
        self.minYEdit.setValue(min_y)
        self.maxYEdit.setValue(max_y)

        # create the map canvas and the toolbar
        self.plot_canvas = MplCanvas(self)
        toolbar = NavigationToolbar(self.plot_canvas, self)

        # add the map canvas and toolbar to the layout
        layout = QVBoxLayout(self.widget)
        layout.addWidget(self.plot_canvas)
        layout.addWidget(toolbar)

        # set the initial colors
        self.colorButton.setColor(QColor(0,0,0))
        self.replicaColorButton.setColor(QColor(0,0,255))

        self.addPlotButton.clicked.connect(self.clickPoint)
        self.addPlotButton.setIcon(QgsApplication.getThemeIcon("mActionAdd.svg"))
        self.clearButton.clicked.connect(self.clearPlot)
        self.clearButton.setIcon(QgsApplication.getThemeIcon("mIconClearItem.svg"))
        self.helpButton.clicked.connect(self.openHelp)
        self.helpButton.setIcon(QgsApplication.getThemeIcon("mActionHelpContents.svg"))
        self.removePlot.clicked.connect(self.removeSelectedPlot)
        self.removePlot.setIcon(QgsApplication.getThemeIcon("mActionRemove.svg"))

        # call the method to fill the title comboboxes
        self.fillComboboxes()

        # connect the widgets to the update methods (follow the UI schema)

        # Scale option
        self.minDateEdit.dateChanged.connect(self.updateXMinDateLimits)
        self.maxDateEdit.dateChanged.connect(self.updateXMaxDateLimits)
        self.minYEdit.valueChanged.connect(self.updateYMinDateLimits)
        self.maxYEdit.valueChanged.connect(self.updateYMaxDateLimits)

        # Plot option
        self.colorButton.colorChanged.connect(self.setPlotFaceColor)
        self.singlePlotRadio.clicked.connect(self.createMultiplePlot)
        self.multiplePlotRadio.clicked.connect(self.createMultiplePlot)

        # Replica
        self.replicaUpCheck.clicked.connect(self.setReplicas)
        self.replicaDistEdit.valueChanged.connect(self.setReplicas)
        self.replicaDownCheck.clicked.connect(self.setReplicas)
        self.replicaColorButton.colorChanged.connect(self.setReplicas)

        # Chart options
        self.hGridCheck.clicked.connect(self.updateGrids)
        self.vGridCheck.clicked.connect(self.updateGrids)
        self.linesCheck.clicked.connect(self.createLinePlot)
        self.labelsCheck.stateChanged.connect(self.hideLabels)
        self.linRegrCheck.clicked.connect(self.createLineRegTrendPlot)
        self.polyRegrCheck.clicked.connect(self.createLinePolyregTrendPlot)
        self.detrendingCheck.clicked.connect(self.createDetrendedPlot)
        self.errorbarCheck.clicked.connect(self.enableErrorBarValue)
        self.errorbarCheck.clicked.connect(self.createErrorbarPlot)
        self.errorBarValue.valueChanged.connect(self.createErrorbarPlot)

        # disable the errorbar check at firts run
        self.errorbarCheck.setEnabled(False)
        self.errorBarValue.setEnabled(False)

        # Chart axis labels
        self.xLabelEdit.textChanged.connect(self.updateLabels)
        self.yLabelEdit.textChanged.connect(self.updateLabels)

        # Chart title
        self.titleParam0Edit.textChanged.connect(self.updateTitle)
        self.titleParam1Edit.textChanged.connect(self.updateTitle)
        self.titleParam2Edit.textChanged.connect(self.updateTitle)
        self.titleParam0Combo.currentTextChanged.connect(self.updateTitle)
        self.titleParam1Combo.currentIndexChanged.connect(self.updateTitle)
        self.titleParam2Combo.currentIndexChanged.connect(self.updateTitle)

        # call the create plot function
        self.createPlot(self.fid)

        # calling the refresh method for the legend AFTER the plot creation
        self.legendCheck.clicked.connect(self.refresh)

        # call the methods to update the title and the labels
        self.updateTitle()
        self.updateLabels()

        self.plotListCombo.currentIndexChanged.connect(self.setDialogWidgetsState)

        self.secondaryLayerCombo.setLayer(layer)
        self.secondaryLayerCombo.setFilters(Qgis.LayerFilter.VectorLayer)
        self.secondaryLayerCombo.layerChanged.connect(lambda: self.xExpressionCombo.setLayer(self.secondaryLayerCombo.currentLayer()))
        self.secondaryLayerCombo.layerChanged.connect(lambda: self.yExpressionCombo.setLayer(self.secondaryLayerCombo.currentLayer()))

        self.colorExpressionCombo.setLayer(self.layer)
        self.updateColorPlotButton.clicked.connect(self.setDataDefinedColor)
        self.updateColorPlotButton.setIcon(QgsApplication.getThemeIcon("mActionRefresh.svg"))
        self.removeColorPlotButton.setIcon(QgsApplication.getThemeIcon("mActionRemove.svg"))
        self.removeColorPlotButton.clicked.connect(self.removeDataDefinedColor)

        self.updatePlotButton.clicked.connect(self.createDoubleYPlot)
        self.updatePlotButton.setIcon(QgsApplication.getThemeIcon("mActionRefresh.svg"))
        self.removePlotButton.clicked.connect(self.removeDoubleYPlot)
        self.removePlotButton.setIcon(QgsApplication.getThemeIcon("mActionRemove.svg"))

        self.rejected.connect(self.cleanOnClosing)

    def cleanOnClosing(self):
        """
        clean the selection when closing the dialog
        """
        self.layer.selectByIds([])

    def openHelp(self):
        """
        open he help webpage
        """
        webbrowser.open('https://gitlab.com/faunalia/ps-speed/-/blob/master/README.md')

    def createDoubleYPlot(self):
        """
        create the double Y plot
        """

        # get the current layer from the combobox
        layer = self.secondaryLayerCombo.currentLayer()

        # set the expression context where to evaluate the expressions
        context = QgsExpressionContext(QgsExpressionContextUtils.globalProjectLayerScopes(layer))

        # build the x expression and stop if not valid
        x_expression = QgsExpression(self.xExpressionCombo.asExpression())
        if not x_expression.isValid():
            self.message_bar.pushMessage('PS Time Series', self.tr('The X expression is no valid'), Qgis.Critical)
            return

        # build the y expression and stop if not valid
        y_expression = QgsExpression(self.yExpressionCombo.asExpression())
        if not y_expression.isValid():
            self.message_bar.pushMessage('PS Time Series', self.tr('The Y expression is no valid'), Qgis.Critical)
            return

        # initialize the empty lists for the final values
        secondary_x_values = []
        secondary_y_values = []

        # set up the request to optimize the loop
        request = QgsFeatureRequest()
        request.setFlags(QgsFeatureRequest.NoGeometry)

        # loop into the vector layer and evaluate the expressions
        for feature in layer.getFeatures(request):
            context.setFeature(feature)
            x_value = x_expression.evaluate(context)
            secondary_x_values.append(x_value)
            y_value = y_expression.evaluate(context)
            secondary_y_values.append(y_value)

        # try to convert the x values in dates and return if failed
        try:
            secondary_x_values = [i.toPyDate() for i in secondary_x_values]
        except:
            self.message_bar.pushMessage('PS Time Series', self.tr('X values are not dates. Please convert them using the to_date expression of the Field Calulator of QGIS'), Qgis.Critical)
            return

        # remove the existing Y plot (if any) by calling the method
        self.removeDoubleYPlot()

        # twin the x axis
        self.twin = self.plot_canvas.axes.twinx()
        # add to the plots dictionary the real scatter with the variables
        self.plots['double_y_plot']['plot'] = self.twin.scatter(secondary_x_values, secondary_y_values, color='blue')
        self.plots['double_y_plot']['x'] = secondary_x_values
        self.plots['double_y_plot']['y'] = secondary_y_values

        # set label title and text
        self.twin.set_ylabel(f'{self.yExpressionCombo.currentText()}', color='blue')
        self.twin.tick_params(axis='y', labelcolor='blue')

        self.updateLimits()
        self.refresh()

    def removeDoubleYPlot(self):
        """
        remove the double plot (if any)
        """
        if self.plots.get('double_y_plot'):
            self.plots['double_y_plot']['plot'].axes.set_yticks([])
            self.plots['double_y_plot']['plot'].axes.set_ylabel('')
            self.plots['double_y_plot']['plot'].remove()
            self.plots.pop('double_y_plot')

        self.updateLimits()
        self.refresh()

    def setDataDefinedColor(self):
        """
        set the color depending on the field chosen

        WARNING: the layer chosen to make the plot must be always the same!
        """

        # create an empty array
        color_values = np.array([])

        # set the expression context where to evaluate the expressions
        context = QgsExpressionContext(QgsExpressionContextUtils.globalProjectLayerScopes(self.layer))

        # build the color expression and stop if not valid
        color_expression = QgsExpression(self.colorExpressionCombo.asExpression())
        if not color_expression.isValid():
            self.message_bar.pushMessage('PS Time Series', self.tr('The color expression is no valid'), Qgis.Critical)
            return

        # loop into the dictionary keys and take the feature
        for k in self.plots.keys():

            if k not in ('double_y_plot', 'average'):

                # get the feature from the plot id
                feature = self.layer.getFeature(k)

                # evaluate the expression
                context.setFeature(feature)
                value = color_expression.evaluate(context)

                # append the value to the numpy array
                color_values = np.append(color_values, value)

        # get the min and max values
        min_val = color_values.min()
        max_val = color_values.max()

        # create the rainbow color scale
        colors = matplotlib.cm.rainbow((color_values - min_val) / (max_val - min_val))

        # loop into the plots dictionary and set teh color
        for i, k in enumerate(self.plots.keys()):
            self.plots[k]['plots']['scatter'].set_facecolors(colors[i])

        # disable the color button
        self.colorButton.setEnabled(False)

        self.refresh()

    def removeDataDefinedColor(self):
        """
        remove the colorscale and get back to the original situation
        """
        # loop on the lot items
        for k, v in self.plots.items():

            if self.plots[k]['plots'].get('scatter'):
                color = self.plots[k]['gui']['colorButton']

                # update the scatterplot
                self.plots[k]['plots']['scatter'].set_facecolor(color.name())

        # enable the color button
        self.colorButton.setEnabled(True)

        self.refresh()

    def getCurrentPlotFid(self):
        """
        utility to get the current fid from the combobox
        FIRST ITEM OF THE USER DATA LIST!
        """
        if self.plotListCombo.currentData():
            plot_fid = self.plotListCombo.currentData()[0]
            return plot_fid

    def setDialogWidgetsState(self):
        """
        set the widgets according to the specification of the layer chosen
        """
        plot_fid = self.getCurrentPlotFid()
        widgets = self.findChildren((QCheckBox, QgsColorButton, QgsSpinBox))

        # update the widget state according to the dictionary
        for widget in widgets:
            # temporary block the signals, else methods are retriggered
            widget.blockSignals(True)
            if widget.objectName() in self.plots[plot_fid]['gui'].keys():
                if isinstance(widget, QCheckBox):
                    # print(widget.objectName(), plot_fid, self.plots[plot_fid]['gui'].get(widget.objectName()))
                    widget.setChecked(self.plots[plot_fid]['gui'].get(widget.objectName()))
                if isinstance(widget, QgsColorButton):
                    # print(widget.objectName())
                    if not self.plots[plot_fid]['gui'].get('propertyButton'):
                        self.colorButton.setEnabled(True)
                        widget.setColor(self.plots[plot_fid]['gui'].get(widget.objectName()))
                if isinstance(widget, QgsSpinBox):
                    widget.setValue(self.plots[plot_fid]['gui'].get(widget.objectName()))
            # unblock the signals to restore the original situation
            widget.blockSignals(False)

        self.refresh()

    def createMultiplePlot(self):
        """
        function to create a single mean values multiple plot.
        This function takes into account the radio button and behave accordingly
        """

        if self.multiplePlotRadio.isChecked():
            for k, v in self.plots.items():
                if k == 'average':
                    continue
                for plots in v['plots'].values():
                    plots.remove()

            xnew = []
            ynew = []

            for k, v in self.plots.items():
                if k == 'average' or k == 'double_y_plot':
                    continue
                tl_x = self.plots[k]['values']['x']
                tl_y = self.plots[k]['values']['y']

                xnew.append(date2num(np.array(tl_x)))
                ynew.append(np.array(tl_y))

            x = np.mean(xnew, axis=0)
            y = np.mean(ynew, axis=0)

            self.plots['average']['values']['x'] = x
            self.plots['average']['values']['y'] = y

            self.multiple_line_plot = self.plot_canvas.axes.plot(x, y, color='black')
            self.plots['average']['plots']['lines'] = self.multiple_line_plot[0]

            # disable the add plot button
            self.addPlotButton.setEnabled(False)

        if self.singlePlotRadio.isChecked():
            if self.plots['average']['plots'].get('lines'):
                self.plots['average']['plots']['lines'].remove()
                self.plots.pop('average')
            for k, v in self.plots.items():
                if not k=='average':
                    for plots in v['plots'].values():
                        self.plot_canvas.axes.add_artist(plots)

            # enable the add plot button
            self.addPlotButton.setEnabled(True)

        self.updateTitle()

    def createErrorbarPlot(self):

        # get x and y from the dictionary
        x = self.plots['average']['values']['x']
        y = self.plots['average']['values']['y']

        # get the error value from the spingbox
        error_value = self.errorBarValue.value()

        # remove the errorbar plot if exists
        if self.plots['average']['plots'].get('errorbar'):
            self.plots['average']['plots'].get('errorbar').remove()
            self.plots['average']['plots'].pop('errorbar')

        if self.errorbarCheck.isChecked():
            self.errorbar_plot = self.plot_canvas.axes.errorbar(x, y, error_value, fmt='.', color='blue', capsize=2.5)
            self.plots['average']['plots']['errorbar'] = self.errorbar_plot
        else:
            self.errorbar_plot.remove()
            self.plots['average']['plots'].pop('errorbar')

        self.refresh()


    def enableMultiplePlotChoice(self):
        """
        enable/disable the multiple radiobutton depending on the plot dictionary keys
        """
        if len(self.plots.keys()) > 1 and not 'average' in self.plots.keys():
            self.multiplePlotRadio.setEnabled(True)
            self.colorExpressionLabel.setEnabled(True)
            self.colorExpressionCombo.setEnabled(True)
            self.updateColorPlotButton.setEnabled(True)
            self.removeColorPlotButton.setEnabled(True)
        else:
            self.multiplePlotRadio.setEnabled(False)
            self.colorExpressionLabel.setEnabled(False)
            self.colorExpressionCombo.setEnabled(False)
            self.updateColorPlotButton.setEnabled(False)
            self.removeColorPlotButton.setEnabled(False)

    def enableErrorBarChoice(self):
        """
        enable the errorbar only when we have multiple plots as a single line
        """
        if self.multiplePlotRadio.isChecked():
            self.errorbarCheck.setEnabled(True)
        else:
            if self.plots['average']['plots'].get('errorbar'):
                self.errorbar_plot.remove()
                self.plots['average']['plots'].pop('errorbar')
            self.errorbarCheck.setEnabled(False)
            self.errorbarCheck.setChecked(False)
            self.errorBarValue.setEnabled(False)

        self.refresh()

    def enableErrorBarValue(self):
        """
        enable the error bar spinbox only when the checkbox is checked
        """
        if self.errorbarCheck.isChecked():
            self.errorBarValue.setEnabled(True)
        else:
            self.errorBarValue.setEnabled(False)

    def createLineTrendPlot(self, fid, d=1):
        """
        calculates the trend values given a certain distance
        """
        # get x and y from the dictionary
        x = self.plots[fid]['values']['x']
        y = self.plots[fid]['values']['y']

        xnew = date2num(np.array(x))
        y = np.array(y)
        p = np.polyfit(xnew, y, d)
        ynew = np.polyval(p, xnew)

        return xnew, ynew

    def createPlot(self, fid, color=None):
        """
        main function that creates the scatter plot
        """

        # get x and y from the dictionary
        x = self.plots[fid]['values']['x']
        y = self.plots[fid]['values']['y']

        # get the color from the button if not None
        if not color:
            color = self.colorButton.color().name()
            self.plots[fid]['gui']['colorButton'] = self.colorButton.color()

        self.scatter = self.plot_canvas.axes.scatter(x, y, marker='s', color=color, edgecolors=color, label=f'{fid}')
        self.plots[fid]['plots']['scatter'] = self.scatter

        # dump into the dictionary the state of the widgets
        self.plots[fid]['gui']['linesCheck'] = self.linesCheck.isChecked()
        self.plots[fid]['gui']['linRegrCheck'] = self.linRegrCheck.isChecked()
        self.plots[fid]['gui']['polyRegrCheck'] = self.polyRegrCheck.isChecked()
        self.plots[fid]['gui']['detrendingCheck'] = self.detrendingCheck.isChecked()
        self.plots[fid]['gui']['replicaUpCheck'] = False
        self.plots[fid]['gui']['replicaDownCheck'] = False
        self.plots[fid]['gui']['replicaColorButton'] = self.replicaColorButton.color()
        self.plots[fid]['gui']['replicaDistEdit'] = self.replicaDistEdit.value()

        self.refresh()

        # add the fid value to the combobox
        self.plotListCombo.addItem(f'{self.iface.activeLayer().name()}-{fid}', [fid, self.iface.activeLayer().id()])
        self.plotListCombo.setCurrentIndex(self.plotListCombo.count() - 1)

        # emit the plot created signal
        self.plot_created.emit()

        # select the point clicked on the map
        self.layer.selectByIds([fid], Qgis.SelectBehavior.AddToSelection)

        # call the update title method
        self.updateTitle()

    def createLinePlot(self):
        """
        function to create the line plot linking each point of the scatter plot
        """
        # get the plot fid from the combobox
        plot_fid = self.getCurrentPlotFid()

        # get x and y from the dictionary
        x = self.plots[plot_fid]['values']['x']
        y = self.plots[plot_fid]['values']['y']

        if self.linesCheck.isChecked():
            self.line_plot = self.plot_canvas.axes.plot(x, y, color='black')
            self.plots[plot_fid]['plots']['lines'] = self.line_plot[0]

        else:
            if self.plots[plot_fid]['plots'].get('lines'):
                self.line_plot[0].remove()
                self.plots[plot_fid]['plots'].pop('lines')

        self.plots[plot_fid]['gui']['linesCheck'] = self.linesCheck.isChecked()

        self.refresh()

    def setReplicas(self):
        """
        function to add the replicas (up or down) with the value of the spinbox
        """
        # get the plot fid from the combobox
        plot_fid = self.getCurrentPlotFid()

        # get the distance from the spinbox
        dist = self.replicaDistEdit.value()

        # get the x and y values of the series from the dictionary
        plot_x = self.plots[plot_fid]['values']['x']
        plot_y = self.plots[plot_fid]['values']['y']

        # get the color for the replicas
        replica_color = self.replicaColorButton.color().name()

        # clear the plot dictionary if the dict value exist and clean the plot canvas
        if self.plots[plot_fid]['plots'].get('up_replicas'):
            self.plots[plot_fid]['plots']['up_replicas'].remove()
            self.plots[plot_fid]['plots'].pop('up_replicas')
            self.plots[plot_fid]['values'].pop('y_up')

        if self.plots[plot_fid]['plots'].get('down_replicas'):
            self.plots[plot_fid]['plots']['down_replicas'].remove()
            self.plots[plot_fid]['plots'].pop('down_replicas')
            self.plots[plot_fid]['values'].pop('y_down')

        # create the plots depending on the check made
        if self.replicaUpCheck.isChecked():
            y = list(map(lambda v: v+dist, plot_y))
            self.up_trend_plot = self.plot_canvas.axes.scatter(plot_x, y, marker='s', color=replica_color)
            self.plots[plot_fid]['plots']['up_replicas'] = self.up_trend_plot
            self.plots[plot_fid]['values']['y_up'] = y
            self.plots[plot_fid]['values']['y_mm'] = dist

        if self.replicaDownCheck.isChecked():
            y = list(map(lambda v: v-dist, plot_y))
            self.down_trend_plot = self.plot_canvas.axes.scatter(plot_x, y, marker='s', color=replica_color)
            self.plots[plot_fid]['plots']['down_replicas'] = self.down_trend_plot
            self.plots[plot_fid]['values']['y_down'] = y
            self.plots[plot_fid]['values']['y_mm'] = dist

        # add the checkbox state to the gui key of the dictionary
        self.plots[plot_fid]['gui']['replicaUpCheck'] = self.replicaUpCheck.isChecked()
        self.plots[plot_fid]['gui']['replicaDownCheck'] = self.replicaDownCheck.isChecked()
        self.plots[plot_fid]['gui']['replicaColorButton'] = self.replicaColorButton.color()
        self.plots[plot_fid]['gui']['replicaDistEdit'] = self.replicaDistEdit.value()

        # call the updateLimits method to get the correct ymin and ymax to set to the plot
        self.updateLimits()

        self.refresh()

    def createLineRegTrendPlot(self):
        """
        function to create the linear trend line by calling the createLineTrendPlot with
        the d=1 parameter
        """
        # get the plot fid from the combobox
        plot_fid = self.getCurrentPlotFid()

        if self.plots[plot_fid]['plots'].get('line_trend'):
            self.plots[plot_fid]['plots']['line_trend'].remove()
            self.plots[plot_fid]['plots'].pop('line_trend')

        if self.linRegrCheck.isChecked():
            xnew, ynew = self.createLineTrendPlot(plot_fid, 1)
            self.trend_line_plot = self.plot_canvas.axes.plot(xnew, ynew, color='red')
            self.plots[self.fid]['plots']['line_trend'] = self.trend_line_plot[0]

        self.plots[plot_fid]['gui']['linRegrCheck'] = self.linRegrCheck.isChecked()

        self.refresh()

    def createLinePolyregTrendPlot(self):
        """
        function to create the polynomial trend line by calling the createLineTrendPlot with
        the d=3 parameter
        """
        # get the plot fid from the combobox
        plot_fid = self.getCurrentPlotFid()

        if self.plots[plot_fid]['plots'].get('poly_trend'):
            self.plots[plot_fid]['plots']['poly_trend'].remove()
            self.plots[plot_fid]['plots'].pop('poly_trend')

        if self.polyRegrCheck.isChecked():
            xnew, ynew = self.createLineTrendPlot(plot_fid, 3)
            self.poly_trend_line_plot = self.plot_canvas.axes.plot(xnew, ynew, color='red')
            self.plots[self.fid]['plots']['poly_trend'] = self.poly_trend_line_plot[0]

        self.plots[plot_fid]['gui']['polyRegrCheck'] = self.polyRegrCheck.isChecked()

        self.refresh()

    def createDetrendedPlot(self):
        """
        function to create the detrended scatter plot. More tricky compare to the other plot
        functions because we need to remove the original plot id the detrended one is chosen
        """
        # get the plot fid from the combobox
        plot_fid = self.getCurrentPlotFid()

        # clear the plot dictionary if the dict value exist and clean the plot canvas
        if self.plots[plot_fid]['plots'].get('detrended'):
            self.plots[plot_fid]['plots']['detrended'].remove()
            self.plots[plot_fid]['plots'].pop('detrended')

        # get the color from the button
        color = self.colorButton.color().name()

        # if detrended then the original scatterplot should be removed!
        if self.detrendingCheck.isChecked():
            if self.plots[plot_fid]['plots'].get('scatter'):
                self.plots[plot_fid]['plots']['scatter'].remove()
            y = np.array(self.y) - np.array(self.createLineTrendPlot(plot_fid, 1)[1])
            self.scatter_detrended = self.plot_canvas.axes.scatter(self.x, y, marker='s', color=color, label=f'Detrended {self.fid}')
            self.plots[plot_fid]['plots']['detrended'] = self.scatter_detrended
        else:
            self.createPlot(plot_fid)

        self.refresh()

    def removeSelectedPlot(self):
        """
        remove the plot chosen from the combobox
        """
        # get plot_fid and layer ids
        plot_fid = self.getCurrentPlotFid()
        layer_id = self.plotListCombo.currentData()[1]

        # create the layer instance
        layer = QgsProject.instance().mapLayer(layer_id)

        for k, v in self.plots[plot_fid]['plots'].items():
            v.remove()
            # remove the plot key from the dictionary if there
            if self.plots.get(plot_fid):
                self.plots.pop(plot_fid)

        # remove the item from the combobox
        self.plotListCombo.removeItem(self.plotListCombo.currentIndex())

        # remove the id from the selection
        layer.selectByIds([plot_fid], Qgis.SelectBehavior.RemoveFromSelection)

        self.refresh()

        # emit the plot created signal
        self.plot_created.emit()

        # call the update title method
        self.updateTitle()

        if not self.plotListCombo.count():
            self.removePlot.setEnabled(False)

    def refresh(self):
        """
        refresh the plot canvas and checks if the legend should be visible
        """
        if self.legendCheck.isChecked():
            self.plot_canvas.axes.legend()
        else:
            if self.plot_canvas.axes.get_legend():
                self.plot_canvas.axes.get_legend().remove()

        self.plot_canvas.draw()

    def clearPlot(self):
        """
        clear all the plots at once
        """

        plot_fid = self.getCurrentPlotFid()
        if self.plotListCombo.count():

            # call the method to remove the double Y plot (if any)
            self.removeDoubleYPlot()

            # loop into the main dictionary and remove the plots
            for k, v in self.plots.items():
                for plot in v['plots'].values():
                    if plot:
                        # since we can have PathCollections or Line2D plots go with try except
                        try:
                            plot.remove()
                        except ValueError as e:
                            pass

            # clear the dictionary
            self.plots.clear()

            self.plotListCombo.clear()

        # set the radio buttons to the initial state
        self.singlePlotRadio.setChecked(True)
        self.multiplePlotRadio.setEnabled(False)

        # call the refresh method
        self.refresh()

    def clickPoint(self):
        """
        function that allows to click on a feature of the active layer
        """
        # get the main map canvas
        self.canvas = self.iface.mapCanvas()

        # get the active layer
        ps_layer = self.iface.activeLayer()

        # create empty messagebox
        message_box = QMessageBox()

        # QMessageBox depending on the number of features selected
        if ps_layer != self.layer:
            message_box = QMessageBox.question(
                self,
                'PS Time Series Viewer',
                self.tr(f'The acive layer is different from the first one. Do you want to proceed?'),
                QMessageBox.Yes | QMessageBox.No, QMessageBox.No
            )

        # proceed only if Yes or the feature count is less than 20
        if message_box == QMessageBox.Yes or ps_layer == self.layer:

            # hide the dialog window
            self.hide()

            # create the pointTool to identify a feature of the layer
            self.pointTool = CustomRectangleMapTool(self.canvas, ps_layer)

            # adds the pointTool to the map canvas
            self.canvas.setMapTool(self.pointTool)

            # connect the identified feature signal to the method
            self.pointTool.canvasClicked.connect(partial(self._onPointClicked, ps_layer))

            self.layer = ps_layer

            self.removePlot.setEnabled(True)

    def _onPointClicked(self, ps_layer, features):
        """
        function performed after the feature identification
        """

        # show the dialog window again
        self.show()

        # create empty messagebox
        message_box = QMessageBox()

        # QMessageBox depending on the number of features selected
        if len(features) > 20:
            message_box = QMessageBox.question(
                self,
                'PS Time Series Viewer',
                self.tr(f'You have selected {len(features)} features. Are you sure to proceed?'),
                QMessageBox.Yes | QMessageBox.No, QMessageBox.No
            )

        # proceed only if Yes or the feature count is less than 20
        if message_box == QMessageBox.Yes or len(features) <= 20:

            self.multiplePlotRadio.setChecked(False)
            self.singlePlotRadio.setChecked(True)
            self.linRegrCheck.setChecked(False)
            self.polyRegrCheck.setChecked(False)

            for feature in features:
                x, y, fieldMap, self.fid, self.structure = getXYvalues(ps_layer, feature)

                # stop if something went wrong with the data
                if not x or not y:
                    self.iface.messageBar().pushMessage(
                        'PS Time Series Viewer',
                        self.tr('Time series not found in the layer, please check carefully the data'),
                        Qgis.Critical
                    )
                    return

                # fill the dictionary with the new x and y values
                self.plots[self.fid]['values']['x'] = x
                self.plots[self.fid]['values']['y'] = y

                color = f"#{random.randint(0, 0xFFFFFF):06x}"
                self.plots[self.fid]['gui']['colorButton'] = QColor(color)

                self.createPlot(self.fid, color)

                self.colorButton.setColor(QColor(color))

        self.canvas.unsetMapTool(self.pointTool)

    def fillComboboxes(self):
        """
        fill the title comboboxes from the fieldMap items
        """
        for k, v in self.fieldMap.items():
            self.titleParam0Combo.addItem(v.name())
            self.titleParam1Combo.addItem(v.name())
            self.titleParam2Combo.addItem(v.name())

    def setPlotFaceColor(self):
        """
        redraw the scatter plot with the chosen color
        """
        # get the color from the button
        color = self.colorButton.color().name()

        # get the current fid from the combobox
        plot_fid = self.getCurrentPlotFid()

        # update the scatterplot
        self.plots[plot_fid]['plots']['scatter'].set_facecolor(color)
        self.plots[plot_fid]['plots']['scatter'].set_edgecolor(color)
        self.plots[plot_fid]['gui']['colorButton'] = self.colorButton.color()
        self.refresh()

    def updateGrids(self):
        """
        show/hide the horizontal or vertical grids
        """
        hgrid = self.hGridCheck.isChecked()
        vgrid = self.vGridCheck.isChecked()

        self.plot_canvas.axes.xaxis.grid(hgrid, 'major')
        self.plot_canvas.axes.yaxis.grid(vgrid, 'major')
        self.refresh()

    def hideLabels(self):
        """
        show/hide the plot x/y labels
        """
        if self.labelsCheck.isChecked():
            self.updateLabels()
        else:
            self.plot_canvas.axes.set_xlabel('')
            self.plot_canvas.axes.set_ylabel('')
            self.refresh()

    def updateLabels(self):
        """
        update the x or y label from the text changed
        """
        # get the text from the widgets
        xtext = self.xLabelEdit.text()
        ytext = self.yLabelEdit.text()

        # update the plot x or y labels
        self.plot_canvas.axes.set_xlabel(xtext)
        self.plot_canvas.axes.set_ylabel(ytext)
        self.refresh()

    def updateXMinDateLimits(self):
        """
        update the x plot limits depending on the minDateEdit widget value
        """
        x_min = self.minDateEdit.date().toPyDate()
        self.plot_canvas.axes.set_xlim(left=x_min)
        self.refresh()

    def updateXMaxDateLimits(self):
        """
        update the x plot limits depending on the maxDateEdit widget value
        """
        x_max = self.maxDateEdit.date().toPyDate()
        self.plot_canvas.axes.set_xlim(right=x_max)
        self.refresh()

    def updateYMinDateLimits(self):
        """
        update the y plot limits depending on the minYEdit widget value
        """
        y_min = self.minYEdit.value()
        self.plot_canvas.axes.set_ylim(bottom=y_min)
        self.refresh()

    def updateYMaxDateLimits(self):
        """
        update the y plot limits depending on the maxYEdit widget value
        """
        y_max = self.maxYEdit.value()
        self.plot_canvas.axes.set_ylim(top=y_max)
        self.refresh()

    def updateLimits(self):
        """
        generic update axis limits depending on the dictionary values for each plot
        """
        # get the values fro the widgets (so they can be safely been overwritten)
        x_min = self.minDateEdit.date().toPyDate()
        x_max = self.maxDateEdit.date().toPyDate()
        y_min = self.minYEdit.value()
        y_max = self.maxYEdit.value()

        # create the x list to get the maximum values
        x_max_list = []
        for k, v in self.plots.items():
            # for the dict structure, avoid the double_y_plot key and append the x values of each plot
            if k not in ('double_y_plot', 'average'):
                x_max_list.append(self.plots[k]['values'].get('x'))

        # if a double plot is there append the double plot values
        if self.plots.get('double_y_plot'):
            x_max_list.append(self.plots['double_y_plot'].get('x'))

        # only if the list exists, get the max value among all the lists
        if x_max_list:
            x_max = max(max(i for i in x_max_list))

        # create the x list to get the maximum values
        x_min_list = []
        for k, v in self.plots.items():
            # for the dict structure, avoid the double_y_plot key and append the x values of each plot
            if k not in ('double_y_plot', 'average'):
                x_min_list.append(self.plots[k]['values'].get('x'))

        # if a double plot is there append the double plot values
        if self.plots.get('double_y_plot'):
            x_min_list.append(self.plots['double_y_plot'].get('x'))

        # only if the list exists, get the max value among all the lists
        if x_min_list:
            x_min = min(min(i for i in x_min_list))

        # list that will be filled with the y_up replicas of all fids
        y_max_list = []
        for k, v in self.plots.items():
            if k not in ('double_y_plot', 'average'):
                if self.plots[k]['values'].get('y_up'):
                    y_max_list.append(self.plots[k]['values'].get('y_up'))
                    y_max_list.append(self.plots[k]['values'].get('y'))


        if self.plots.get('double_y_plot'):
            y_max_list.append(self.plots['double_y_plot'].get('y'))

        # only if the list exists, get the max value among all the lists
        if y_max_list:
            y_max = max(max(i for i in y_max_list))

        # list that will be filled with the y_up replicas of all fids
        y_min_list = []
        for k, v in self.plots.items():
            if k not in ('double_y_plot', 'average'):
                if self.plots[k]['values'].get('y_down'):
                    y_min_list.append(self.plots[k]['values'].get('y_down'))
                    y_min_list.append(self.plots[k]['values'].get('y'))

        if self.plots.get('double_y_plot'):
            y_min_list.append(self.plots['double_y_plot'].get('y'))

        # only if the list exists, get the min value among all the lists
        if y_min_list:
            y_min = min(min(i for i in y_min_list))

        # set also widgets values
        self.minYEdit.setValue(y_min)
        self.maxYEdit.setValue(y_max)

        # update the plot limits
        self.plot_canvas.axes.set_xlim(x_min, x_max)
        self.plot_canvas.axes.set_ylim(y_min, y_max)
        self.refresh()

    def updateTitle(self):
        """
        update the plot title from the widgets
        """

        plot_fid = self.getCurrentPlotFid()

        # return to avoid stack trace when the plot combobox is empty
        if not plot_fid:
            return

        if self.plots.get('average'):
            self.plot_canvas.axes.set_title(f'''Mean plot of x PS''')

        elif len(self.plots.keys()) > 1 and not self.plots.get('average'):
            self.plot_canvas.axes.set_title(f'''{len(self.plots.keys())} points selected''')

        else:
            # get the title labels from the widgets
            label_title_param0 = self.titleParam0Edit.text()
            label_title_param1 = self.titleParam1Edit.text()
            label_title_param2 = self.titleParam2Edit.text()

            # get the currentText from the field widgets
            combo_title_param0 = self.titleParam0Combo.currentText()
            combo_title_param1 = self.titleParam1Combo.currentText()
            combo_title_param2 = self.titleParam2Combo.currentText()

            # set up the request to only filter the desired fid and optimize with NoGeometry
            request = QgsFeatureRequest()
            request.setFilterFid(plot_fid)
            request.setFlags(QgsFeatureRequest.NoGeometry)

            # loop into the activeLayer and get the correct values
            for feature in self.iface.activeLayer().getFeatures(request):
                val_title0 = feature[f"{combo_title_param0}"]
                val_title1 = feature[f"{combo_title_param1}"]
                val_title2 = feature[f"{combo_title_param2}"]

            # set the title of the plot
            self.plot_canvas.axes.set_title(f'{label_title_param0} {val_title0} {label_title_param1} {val_title1} {label_title_param2} {val_title2}')
        self.refresh()

    def tr(self, message):
        # noinspection PyTypeChecker,PyArgumentList,PyCallByClass
        return QCoreApplication.translate('', message)