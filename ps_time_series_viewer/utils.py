# -*- coding: utf-8 -*-

"""
/***************************************************************************
Name                : PS Time Series Viewer
Description         : Computation and visualization of time series of speed for
                    Permanent Scatterers derived from satellite interferometry
Date                : Jul 25, 2012
copyright           : (C) 2012 by Giuseppe Sucameli and Matteo Ghetta (Faunalia)
email               : brush.tyler@gmail.com matteo.ghetta@faunalia.it

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

from qgis.PyQt.QtCore import QDate, pyqtSignal
from qgis.PyQt.QtGui import QColor

from qgis.core import (
    QgsFeatureRequest,
    QgsWkbTypes,
    QgsPointXY,
    QgsRectangle,
    NULL
)

from qgis.gui import (
    QgsMapToolIdentifyFeature,
    QgsMapToolEmitPoint,
    QgsMapTool,
    QgsRubberBand
)

import re

def getXYvalues(ps_layer, feature):

    layer = ps_layer
    fid = feature.id()
    attrs = feature.attributes()

    x, y = [], []	# lists containg x,y values
    infoFields = {}	# hold the index->name of the fields containing info to be displayed

    ps_source = ps_layer.source()
    ps_fields = ps_layer.fields()

    providerType = ps_layer.providerType()
    uri = ps_source
    subset = ""

    pattern1 = r'^[dD]\d{8}$'
    pattern2 = r'\d{8}$'

    if providerType in ('ogr', 'delimitedtext', 'postgres', 'spatialite'):
        for idx, fld in enumerate(ps_fields):
            if attrs[ idx ] != NULL:
                if re.match(pattern1, fld.name()) or re.match(pattern2, fld.name()):
                    if len(fld.name()) == 9:
                        x.append(QDate.fromString(fld.name()[1:], "yyyyMMdd").toPyDate())
                    else:
                        x.append(QDate.fromString(fld.name(), "yyyyMMdd").toPyDate())
                    y.append(float(attrs[ idx ]))
                else:
                    infoFields[ idx ] = fld

    return x, y, infoFields, fid, layer

class CustomRectangleMapTool(QgsMapToolIdentifyFeature, QgsMapToolEmitPoint):
    rectangleCreated = pyqtSignal()
    deactivated = pyqtSignal()
    canvasClicked = pyqtSignal(list)

    def __init__(self, canvas, layer):
        self.canvas = canvas
        self.layer = layer
        QgsMapToolIdentifyFeature.__init__(self, canvas, self.layer)
        QgsMapToolEmitPoint.__init__(self, self.canvas)

        self.rubberBand = QgsRubberBand(self.canvas, QgsWkbTypes.GeometryType.PolygonGeometry)
        self.rubberBand.setColor(QColor(255, 0, 0, 100))
        self.rubberBand.setWidth(2)

        # clear the map from the rubberband
        self.reset()

    def reset(self):
        """
        some standard method and variable to clear the map and the rubberband
        """
        self.startPoint = self.endPoint = None
        self.isEmittingPoint = False
        self.rubberBand.reset(QgsWkbTypes.GeometryType.PolygonGeometry)

    def canvasPressEvent(self, e):
        """
        the click event. Convert the mouse click to map canvas coordinates
        """

        self.clicked_point = self.toLayerCoordinates(self.layer, e.mapPoint())
        features_identified = self.identify(e.x(), e.y())

        features = []
        for feature in features_identified:
            features.append(feature.mFeature)
        self.startPoint = self.toMapCoordinates(e.pos())
        self.endPoint = self.startPoint
        self.isEmittingPoint = True

        self.canvasClicked.emit(features)

        # call the method to show the rectangle on the map
        self.showRect(self.startPoint, self.endPoint)

    def canvasReleaseEvent(self, e):
        """
        the canvas release event (final click on the map)
        """
        request = QgsFeatureRequest()

        self.isEmittingPoint = False
        if self.rectangle() is not None:
            self.rectangleCreated.emit()

            rectangle = self.toLayerCoordinates(self.layer, self.rectangle())
            request.setFilterRect(rectangle)

            features = [feature for feature in self.layer.getFeatures(request)]

            self.canvasClicked.emit(features)

        # clean the map from the rectangle
        self.reset()

    def canvasMoveEvent(self, e):
        """
        the move event, when the mouse is moved around the map
        """
        if not self.isEmittingPoint:
            return

        self.endPoint = self.toMapCoordinates(e.pos())
        self.showRect(self.startPoint, self.endPoint)

    def showRect(self, startPoint, endPoint):
        """
        fill the rubberband polygon as a rectangle from the clicked points
        """
        self.rubberBand.reset(QgsWkbTypes.GeometryType.PolygonGeometry)
        if startPoint:
            if startPoint.x() == endPoint.x() or startPoint.y() == endPoint.y():
                return

            point1 = QgsPointXY(startPoint.x(), startPoint.y())
            point2 = QgsPointXY(startPoint.x(), endPoint.y())
            point3 = QgsPointXY(endPoint.x(), endPoint.y())
            point4 = QgsPointXY(endPoint.x(), startPoint.y())

            self.rubberBand.addPoint(point1, False)
            self.rubberBand.addPoint(point2, False)
            self.rubberBand.addPoint(point3, False)
            # True to update canvas
            self.rubberBand.addPoint(point4, True)
            self.rubberBand.show()

    def rectangle(self):
        """
        return the rectangle from the rubberband
        """
        if self.startPoint is None or self.endPoint is None:
            return None
        elif self.startPoint.x() == self.endPoint.x() or self.startPoint.y() == self.endPoint.y():
            return None

        return QgsRectangle(self.startPoint, self.endPoint)

    def setRectangle(self, rect):
        if rect == self.rectangle():
            return False

        if rect is None:
            self.reset()
        else:
            self.startPoint = QgsPointXY(rect.xMaximum(), rect.yMaximum())
            self.endPoint = QgsPointXY(rect.xMinimum(), rect.yMinimum())
            self.showRect(self.startPoint, self.endPoint)
        return True

    def deactivate(self):
        QgsMapTool.deactivate(self)
        self.deactivated.emit()
